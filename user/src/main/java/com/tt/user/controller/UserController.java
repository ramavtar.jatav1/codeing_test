package com.tt.user.controller;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tt.user.dto.UserDTO;
import com.tt.user.model.User;
import com.tt.user.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createUser(@RequestBody UserDTO userDTO) {

		return new ResponseEntity<>(userService.createUser(userDTO), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> getById(@PathVariable String id) {

		return new ResponseEntity<>(userService.findUserById(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return new ResponseEntity<>(userService.getAllUser(), HttpStatus.OK);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Boolean> updateUser(@PathVariable String id, @RequestBody User user) {
		return new ResponseEntity<Boolean>(userService.updateUser(user), HttpStatus.valueOf(200));
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteUser(@PathVariable String id) {
		return new ResponseEntity<>(userService.deleteUserById(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/exists/{id}", method = RequestMethod.GET)
	public ResponseEntity<Boolean> isUserExists(@PathVariable String id) {
		return new ResponseEntity<>(userService.isUserExists(id), HttpStatus.OK);
	}

}
