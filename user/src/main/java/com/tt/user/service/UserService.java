package com.tt.user.service;

import java.util.List;

import com.tt.user.dto.UserDTO;
import com.tt.user.model.User;

public interface UserService {

	boolean isUserExists(String id);

	UserDTO findUserById(String id);

	Boolean deleteUserById(String id);

	Boolean createUser(UserDTO user);

	Boolean updateUser(User user);

	List<UserDTO> getAllUser();

}
