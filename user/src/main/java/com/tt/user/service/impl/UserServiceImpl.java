package com.tt.user.service.impl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tt.user.dto.UserDTO;
import com.tt.user.model.User;
import com.tt.user.repository.UserRepository;
import com.tt.user.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public boolean isUserExists(String id) {
		return userRepository.isUserExists(id) == id ? true : false;
	}

	@Override
	public UserDTO findUserById(String id) {
		return transformUsertoUserDTO(userRepository.findById(id));
	}

	@Override
	public Boolean deleteUserById(String id) {
		return userRepository.deleteById(id) != 0 ? true : false;
	}

	@Override
	public Boolean createUser(UserDTO userDTO) {
		try {
			userRepository.insert(transformUserDTOtoUser(userDTO));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public Boolean updateUser(User user) {
		return userRepository.update(user) != 0 ? true : false;
	}

	@Override
	public List<UserDTO> getAllUser() {
		return userRepository.findAll().stream().map(user -> transformUsertoUserDTO(user)).collect(Collectors.toList());
	}

	User transformUserDTOtoUser(UserDTO userDTO) {
		User user = new User();
		user.setId(userDTO.getId());
		user.setCreatedBy(userDTO.getCreatedBy());
		LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(userDTO.getCreatedTime()),
				ZoneId.systemDefault());
		user.setCreatedTime(localDateTime);
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setLastUpdatedBy(userDTO.getLastUpdatedBy());
		LocalDateTime lastUpdate = LocalDateTime.ofInstant(Instant.ofEpochMilli(userDTO.getCreatedTime()),
				ZoneId.systemDefault());
		user.setLastUpdatedTime(lastUpdate);
		user.setPicture(userDTO.getPicture());
		user.setPreferredLanguage(userDTO.getPreferredLanguage());
		user.setBackgroundCheck(userDTO.getBackgroundCheck());
		return user;

	}

	UserDTO transformUsertoUserDTO(User user) {
		if (user == null)
			return null;
		UserDTO userDTO = new UserDTO();

		userDTO.setId(user.getId());
		userDTO.setCreatedBy(user.getCreatedBy());
		userDTO.setCreatedTime(user.getCreatedTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setLastUpdatedBy(user.getLastUpdatedBy());
		userDTO.setLastUpdatedTime(user.getLastUpdatedTime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
		userDTO.setPicture(user.getPicture());
		userDTO.setPreferredLanguage(user.getPreferredLanguage());
		userDTO.setBackgroundCheck(user.getBackgroundCheck());
		return userDTO;

	}

}
