package com.tt.user.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.tt.user.model.User;

@Mapper
public interface UserRepository {
	@Select("SELECT id , created_by as createdBy , created_time as createdTime , last_updated_by as lastUpdatedBy ,"  
		   + " last_updated_time as lastUpdatedTime , first_name as firstName , last_name as lastName , picture , " 
    		+ " preferred_language as preferredLanguage , background_check as backgroundCheck from users")
	public List<User> findAll();

	@Select("SELECT id , created_by as createdBy , created_time as createdTime , last_updated_by as lastUpdatedBy ,"
			+ " last_updated_time as lastUpdatedTime , first_name as firstName , last_name as lastName , picture ,"
			+ " preferred_language as preferredLanguage , background_check as backgroundCheck FROM users WHERE id = #{id}")
	public User findById(String id);

	@Delete("DELETE FROM users WHERE id = #{id}")
	public int deleteById(String id);

	@Insert("INSERT INTO users(id, created_by, created_time , last_updated_by , last_updated_time,  first_name, last_name,picture, preferred_language , background_check) "
			+ " VALUES (#{id},#{createdBy},#{createdTime},#{lastUpdatedBy},#{lastUpdatedTime}, #{firstName}, #{lastName}, #{picture} , #{preferredLanguage}, #{backgroundCheck})")
	public void insert(User user);

	@Update("Update users set first_name=#{firstName}, " + " last_name=#{lastName} where id=#{id}")
	public int update(User user);
	
	@Select("SELECT id  FROM users WHERE id = #{id}")
	public String isUserExists(String id);
}
